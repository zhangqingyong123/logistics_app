//
//  Defines.h
//  QuakeVideo
//
//  Created by weijieMac on 2017/9/15.
//  Copyright © 2017年 Mac. All rights reserved.
//

#ifndef Defines_h
#define Defines_h

//单例模式宏模板
#define DEFINE_SINGLETON_FOR_HEADER(className) \
\
+ (className *)shared##className;

#define DEFINE_SINGLETON_FOR_CLASS(className) \
\
+ (className *)shared##className { \
static className *shared##className = nil; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
shared##className = [[self alloc] init]; \
}); \
return shared##className; \
}
#define C_UserDefaults_Set(key, value) NSUserDefaults *ud = [[NSUserDefaults standardUserDefaults] init]; [ud setObject:value forKey:key]; [ud synchronize];
#define C_UserDefaults_Get(key) [[NSUserDefaults standardUserDefaults] objectForKey:key];
//系统
#define IS_IOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
#define IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)
#define IS_IOS11 ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 11.0)
//获取设备唯一ID
#define DEVICEID [[UIDevice currentDevice].identifierForVendor UUIDString]

//Value
#define RealValue_W(value) ((value)/2.0/375.0f*[UIScreen mainScreen].bounds.size.width)
#define RealValue_H(value) ((value)/2.0/375.0f*[UIScreen mainScreen].bounds.size.width)
//#define RealValue_H(value) ((value)/2.0/667.0f* [UIScreen mainScreen].bounds.size.height)

//Frame
//Frame
#define SCREEN_SIZE [[UIScreen mainScreen]bounds].size
#define SCREEN_FRAME [[UIScreen mainScreen]bounds]
#define PassworldLastlength  6
#define Passworldhighlength  16
#define codelength  6
#define UIScreenWidth SCREEN_SIZE.width
#define UIScreenHeight  SCREEN_SIZE.height
#define SCREEN_CENTER_X SCREEN_SIZE.width/2
#define SCREEN_CENTER_Y SCREEN_SIZE.height/2
#define NAVI_HEIGHT ([FVUnit deviceIsIPhoneX] ? 64 + 24 : 64)

#define IS_IPhoneX_All ([UIScreen mainScreen].bounds.size.height == 812 || [UIScreen mainScreen].bounds.size.height == 896)
#define IPhoneTop ((IS_IPhoneX_All == YES) ? 24 : 0.0)
#define IPhoneTopHeight ((IS_IPhoneX_All == YES) ? 44 : 20)

#define TABBAR_HEIGHT ((IS_IPhoneX_All == YES) ? 50 + 24 : 50)
#define kStatusBarAndNavigationBarHeight ((IS_IPhoneX_All == YES) ? 88.0 : 64.0)
#define ContentViewHeight UIScreenHeight - kStatusBarAndNavigationBarHeight - TABBAR_HEIGHT

#define IPhoneBottom ((IS_IPhoneX_All == YES) ? 16 : 0.0)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_MAX_LENGTH MAX(UIScreenWidth,UIScreenHeight)
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)
#define KIsIPhoneX ((int)((UIScreenHeight/UIScreenWidth)*100) == 216)?YES:NO
#define KAreaInsetsBottom ((KIsIPhoneX == YES)? 34 : 0.0)


//自定义颜色
#define RGBA(R/*红*/, G/*绿*/, B/*蓝*/, A/*透明*/) \
[UIColor colorWithRed:R/255.f green:G/255.f blue:B/255.f alpha:A]

// 字体
#define AppFont(x) [UIFont systemFontOfSize:x]
#define AppBoldFont(x) [UIFont boldSystemFontOfSize:x]

// 颜色
#define AppColor(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define AppAlphaColor(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define AppHTMLColor(html) [UIColor colorWithHexString:html]
//系统颜色
#define WHITECOLOR [UIColor whiteColor]
#define CLEARCOLOR [UIColor clearColor]
#define BLACKCOLOR [UIColor blackColor]
#define ORANGECOLOR [UIColor orangeColor]

#define MAINCOLOR RGBA(44,143,219,1)
#define MAINTITLECOLOR ColorStr(@"#333333")
#define TABTITLECOLOR ColorStr(@"#2B85F3")
#define DEFAULTBACKGROUNDCOLOR RGBA(240,240,240,1)
#define NAVTITLECOLOR RGBA(31,84,139,1)
#define TABNORMALCOLOR RGBA(111, 111, 111, 1)
#define maintitleLCOLOR RGBA(177, 177, 177, 1)
#define colorrefColor(R, G, B, A)  CGColorCreate(colorSpace,(CGFloat[]){ R/255.f, G/255.f, B/255.f, A });
#define ColorStr(a) [WJUnit colorWithHexString: (NSString *)a]
#define RGBColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]

#define RGBAColor(r, g, b ,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:a]



//7.设置 view 圆角和边框
#define KViewBorderRadius(View, Radius, Width, Color)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]

/// View 圆角
#define KViewRadius(View, Radius)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES]

///  View加边框
#define KViewBorder(View, BorderColor, BorderWidth )\
\
View.layer.borderColor = BorderColor.CGColor;\
View.layer.borderWidth = BorderWidth;


//8.字符串是否为空
#define kStringIsEmpty(str) ([str isKindOfClass:[NSNull class]] || str == nil || [str length] < 1 ? YES : NO )

//9.数组是否为空
#define kArrayIsEmpty(array) (array == nil || [array isKindOfClass:[NSNull class]] || array.count == 0)

//10.字典是否为空
#define kDictIsEmpty(dic) (dic == nil || [dic isKindOfClass:[NSNull class]] || dic.allKeys == 0)

//11.是否是空对象
#define kObjectIsEmpty(_object) (_object == nil \
|| [_object isKindOfClass:[NSNull class]] \
|| ([_object respondsToSelector:@selector(length)] && [(NSData *)_object length] == 0) \
|| ([_object respondsToSelector:@selector(count)] && [(NSArray *)_object count] == 0))


#define RandColor RGBColor(arc4random_uniform(255), arc4random_uniform(255), arc4random_uniform(255))


#define EDColor_BaseBlue RGBA(44,143,219,1)
//系统字体
#define SystemFont(size) [UIFont systemFontOfSize:(size)]
#define SystemBlodFont(font)  [UIFont fontWithName:@"Helvetica-Bold"size:font]
//* 适配字体  *//
#define AutoFont(size)     [UIFont systemFontOfSize:size * UIScreenWidth/(375.0f)]

#define AutoBoldFont(font)   [UIFont fontWithName:@"Helvetica-Bold"size:font * UIScreenWidth/(375.0f)]

/// View 圆角
#define KViewRadius(View, Radius)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES]

///  View加边框
#define KViewBorder(View, BorderColor, BorderWidth )\
\
View.layer.borderColor = BorderColor.CGColor;\
View.layer.borderWidth = BorderWidth;


//防止block里引用self造成循环引用
#define WeakObject(obj/*对象*/)  __weak __typeof(obj)weak##obj = obj;

//weakself
#define WeakSelf typeof(self) __weak weakSelf = self;

//strongself
#define StrongSelf __strong __typeof(self) strongSelf = weakSelf;


//1.带有RGBA的颜色设置
#define kRGBColor(r, g, b)    [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
#define kRGBAColor(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(r)/255.0 blue:(r)/255.0 alpha:a]

#define kKRandomColor    KRGBColor(arc4random_uniform(256)/255.0,arc4random_uniform(256)/255.0,arc4random_uniform(256)/255.0)        //随机色生成
//2.rgb颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 \
green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 \
blue:((float)(rgbValue & 0xFF)) / 255.0 alpha:1.0]

//2.清除背景色
#define CLEARCOLOR [UIColor clearColor]

#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]




//沙盒路径
#define LibraryPath [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) firstObject]
#define DocumentPath [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]
#define CachePath [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject]

#define ChatBookStr @"QuakeVideo-ChatBook"

#define ChatBookPath [NSString stringWithFormat:@"%@/%@",DocumentPath,ChatBookStr]


//window
#define  kWindow   [WJUnit KWindow]
//* 站位图  *//
#define PlaceHolderImage   [UIImage imageNamed:@"morentouxiang"]
#define UnreadMessageKey  @"UnreadMessage"

#define UnreadNoticeKey   @"UnreadNoticeKey"

#define bnanerimageURL   @"http://119.81.171.118:5050/" //image拼接地址
#define imageGameURL   @"http://111.177.18.197:8880/richman/home/upload/images/game/" //生产的地址
#define PTimageGameURL   @"http://111.177.18.197:8880/richman/home/upload/images/gamepic/" //生产的地址
//公共
#define kFormat(string, args...)                  [NSString stringWithFormat:string, args]

#endif /* Defines_h */
