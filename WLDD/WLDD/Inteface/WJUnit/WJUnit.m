//
//  WJUnit.m
//  WLDD
//
//  Created by 张庆勇 on 2019/12/7.
//  Copyright © 2019 物流. All rights reserved.
//

#import "WJUnit.h"
#import "sys/utsname.h"
#import "AppDelegate.h"
@implementation WJUnit
+ (UIWindow *)KWindow;
{
    UIWindow* window = nil;
    
    window = [UIApplication sharedApplication].keyWindow;
    
    return window;
 
}
/*
 iphoneX
 */
+ (BOOL)deviceIsIPhoneX {
    struct utsname systemInfo;
    
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
    if ([platform isEqualToString:@"i386"] || [platform isEqualToString:@"x86_64"]) {
        // 模拟器下采用屏幕的高度来判断
        return [UIScreen mainScreen].bounds.size.height == 812;
    }
    // iPhone10,6是美版iPhoneX 感谢hegelsu指出：https://github.com/banchichen/TZImagePickerController/issues/635
    BOOL isIPhoneX = [platform isEqualToString:@"iPhone10,3"] || [platform isEqualToString:@"iPhone10,6"];
    return isIPhoneX;
}

+ (void)showMessage:(NSString *)message
{
    static BOOL isShow = NO;
    if (isShow) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        isShow = YES;
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = AutoFont(14);
        style.verticalPadding =10;
        style.cornerRadius = 4;
        style.messageColor = WHITECOLOR;
        style.backgroundColor = BLACKCOLOR;
       
        
        [kWindow makeToast:message
                  duration:2
                  position:CSToastPositionCenter
                     style:style];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            isShow = NO;
        });
    });
}
// 颜色转换三：iOS中十六进制的颜色（以#开头）转换为UIColor
+ (UIColor *)colorWithHexString:(NSString *)color
{
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    
    // 判断前缀并剪切掉
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    
    // 从六位数值中找到RGB对应的位数并转换
    NSRange range;
    range.location = 0;
    range.length = 2;
    
    //R、G、B
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}
+ (NSMutableAttributedString *)finderattributedString:(NSString *)str attributedString:(NSString *)attributedString color:(UIColor *)color font:(UIFont *)font
{
    
    NSRange range = [str rangeOfString:attributedString];
    NSMutableAttributedString *muattributeString  = [[NSMutableAttributedString alloc]initWithString:str];
    [muattributeString addAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];
    
    
    return muattributeString;
}
+ (NSMutableAttributedString *)finderattributedLinesString:(NSString *)str attributedString:(NSString *)attributedString color:(UIColor *)color font:(UIFont *)font
{
    
    NSRange range = [str rangeOfString:attributedString];
    NSMutableAttributedString *muattributeString  = [[NSMutableAttributedString alloc]initWithString:str];
    NSMutableParagraphStyle *paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setLineSpacing:10];
    [muattributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [str length])];
    [muattributeString addAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];
    
    
    return muattributeString;
}
+ (NSMutableAttributedString *)getcontent:(NSString *)str image:(NSString *)imageStr index:(NSInteger)index imageframe:(CGRect)imageframe
{
    CGFloat y = imageframe.origin.y;
    if (y == 0) {
        y = RealValue_H(1);
    }
    NSMutableAttributedString *attri       = [[NSMutableAttributedString alloc] initWithString:str];
    NSTextAttachment *attch                = [[NSTextAttachment alloc] init];
    attch.image                            = [UIImage imageNamed:imageStr];
    attch.bounds                           = CGRectMake(0,y, imageframe.size.width,imageframe.size.height);
    
    NSAttributedString *string             = [NSAttributedString attributedStringWithAttachment:attch];
    [attri insertAttributedString:string atIndex:index];
    return attri;
}
//获取Window当前显示的ViewController
+ (UIViewController*)currentViewController{
    AppDelegate *appdele = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIViewController *vc  =  appdele.window.rootViewController;
    
    //    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    //    UIViewController* vc = (UIViewController *)window.rootViewController;
    
    if ([vc isKindOfClass:[UITabBarController class]]) {
        vc = ((UITabBarController*)vc).selectedViewController;
    }
    
    if ([vc isKindOfClass:[UINavigationController class]]) {
        vc = ((UINavigationController*)vc).visibleViewController;
    }
    
    if (vc.presentedViewController) {
        vc = vc.presentedViewController;
    }
    
    return vc;
}
+ (CGFloat)getStringHeightWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)width {
    // 设置文字属性 要和label的一致
    NSDictionary *attrs = @{NSFontAttributeName :font};
    CGSize maxSize = CGSizeMake(width, MAXFLOAT);
    
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    
    // 计算文字占据的宽高
    CGSize size = [text boundingRectWithSize:maxSize options:options attributes:attrs context:nil].size;
    
    // 当你是把获得的高度来布局控件的View的高度的时候.size转化为ceilf(size.height)。
    return  ceilf(size.height);
}
@end
