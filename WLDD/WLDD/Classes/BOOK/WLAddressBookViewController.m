//
//  WLAddressBookViewController.m
//  WLDD
//
//  Created by 张庆勇 on 2019/12/7.
//  Copyright © 2019 物流. All rights reserved.
//

#import "WLAddressBookViewController.h"
#import "WLLoginViewController.h"
@interface WLAddressBookViewController ()

@end

@implementation WLAddressBookViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.leftButton setImage:[UIImage imageNamed:@"search"] forState:0];
    self.titleLable = @"通讯录";
    [self.rightButton setImage:[UIImage imageNamed:@"addition"] forState:0];
}
/*搜索*/
- (void)leftItem:(UIButton *)sender
{
    [WLUserManager dj_showLoginVC:self];
}
/*添加*/
- (void)rightItem:(UIButton *)sender
{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
