//
//  WLLoginViewController.h
//  WLDD
//
//  Created by 张庆勇 on 2019/12/7.
//  Copyright © 2019 物流. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLLoginViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIImageView *user_icon;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *pwField;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (weak, nonatomic) IBOutlet UIButton *LoginProblemBtn;
@property (weak, nonatomic) IBOutlet UIButton *phoneLogin;
@property (weak, nonatomic) IBOutlet LcButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@property (weak, nonatomic) IBOutlet UIButton *weChatBtn;
- (IBAction)phoneLoginBtn:(UIButton *)sender;
- (IBAction)codeBtn:(UIButton *)sender;
- (IBAction)registerBtn:(UIButton *)sender;
- (IBAction)loginBtn:(LcButton *)sender;
- (IBAction)problemBtn:(UIButton *)sender;
- (IBAction)weChatBtn:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
