//
//  WLRegisterViewController.h
//  WLDD
//
//  Created by 张庆勇 on 2019/12/9.
//  Copyright © 2019 物流. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLRegisterViewController : BaseViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top_h;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *codeField;
@property (weak, nonatomic) IBOutlet UITextField *pwField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPwField;
@property (weak, nonatomic) IBOutlet UITextField *invitingField;
@property (weak, nonatomic) IBOutlet UIButton *codebtn;
- (IBAction)codeBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet LcButton *registBtn;
- (IBAction)registBtn:(LcButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *agreeLab;

@end

NS_ASSUME_NONNULL_END
