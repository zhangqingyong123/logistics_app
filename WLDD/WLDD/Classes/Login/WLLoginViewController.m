//
//  WLLoginViewController.m
//  WLDD
//
//  Created by 张庆勇 on 2019/12/7.
//  Copyright © 2019 物流. All rights reserved.
//

#import "WLLoginViewController.h"
#import "WLRegisterViewController.h"
@interface WLLoginViewController ()

@end

@implementation WLLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLable = @"登录";
    self.lineView.hidden = NO;
    [self custonUI];
}
- (void)custonUI
{
    self.codeBtn.hidden = YES;
     KViewRadius(_codeBtn, 2);
    [self codebtnCountDown:NO];
    [self initTextFieldWithTextField:_phoneField placeholder:@"输入手机号"];
    [self initTextFieldWithTextField:_pwField placeholder:@"输入密码"];
    KViewRadius(_loginBtn, 2);
    
}
- (IBAction)phoneLoginBtn:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.codeBtn.hidden = !sender.selected;
    if (sender.selected) {
         [self initTextFieldWithTextField:_pwField placeholder:@"输入验证码"];
    }else
    {
        [self initTextFieldWithTextField:_pwField placeholder:@"输入密码"];
    }
}
/*
 *获取验证码
 */
- (IBAction)codeBtn:(UIButton *)sender {
    
    [self codebtnCountDown:YES];
    [self.codeBtn scheduledTimerWithTimeInterval:60 title:@"重新获取" countDownTitle:@"" titleTextColor:TABTITLECOLOR countDownTitleTextColor: RGBA(204, 204, 204, 1) completion:^{
           NSLog(@"结束吗");
        [self codebtnCountDown:NO];
       }];
    
}
- (void)codebtnCountDown:(BOOL)isDown
{
    if (!isDown) {
         KViewBorder(_codeBtn, TABTITLECOLOR, 0.5);
        
    }else
    {
         KViewBorder(_codeBtn, RGBA(204, 204, 204, 1), 0.5);
    }
   
}
- (void)initTextFieldWithTextField:(UITextField *)TextField placeholder:(NSString *)placeholder
{
    TextField.placeholder = placeholder;
    Ivar ivar =  class_getInstanceVariable([UITextField class], "_placeholderLabel");
    UILabel *placeholderLabel = object_getIvar(TextField, ivar);
    placeholderLabel.textColor = RGBA(204, 204, 204, 1);
    placeholderLabel.font = AutoFont(16);
    TextField.tintColor = TABTITLECOLOR;
    TextField.textColor = BLACKCOLOR;
    
}
/*
*注册
*/
- (IBAction)registerBtn:(UIButton *)sender {
    
    [self.navigationController pushViewController:[[WLRegisterViewController alloc] init] animated:YES];
}
/*
*登录
*/
- (IBAction)loginBtn:(LcButton *)sender {
}
/*
*登录常见问题
*/
- (IBAction)problemBtn:(UIButton *)sender {
}
/*
*微信登录
*/
- (IBAction)weChatBtn:(UIButton *)sender {
}
@end
