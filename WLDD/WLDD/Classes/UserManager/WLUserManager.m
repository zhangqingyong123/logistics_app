//
//  WLUserManager.m
//  WLDD
//
//  Created by 张庆勇 on 2019/12/7.
//  Copyright © 2019 物流. All rights reserved.
//

#import "WLUserManager.h"
#import "WLLoginViewController.h"
#import "WLBaseNavigationViewController.h"
@implementation WLUserManager
//获取个人信息
+ (WLUser *)userInfo;
{
    return [WLUser getModelWithPath:@"WLUser"];
}
//保存个人信息
+ (void)saveUserInfo:(WLUser *)userInfo;
{
    [userInfo saveModelWithPath:@"WLUser"];
}
+ (void)loginOut{
    
     [WLUser removeFileByFileName:@"WLUser"];
    
}
//是否登陆
+ (BOOL)is_Login{
    
    if (kStringIsEmpty([self userInfo].userId)) {
        return NO;
    }else{
        return YES;
    }
}

+ (void)dj_showLoginVC:(UIViewController*)toViewController{
    
    WLLoginViewController * LoginMainVC = [[WLLoginViewController alloc] init];
    [toViewController.navigationController pushViewController:LoginMainVC animated:YES];
}
@end
