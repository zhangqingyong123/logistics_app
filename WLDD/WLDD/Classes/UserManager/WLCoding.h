

#import <Foundation/Foundation.h>
//模型继承这个类，调用下方四个方法就可以实现模型的存取
@interface WLCoding : NSObject

/*
 模型调用这个方法，存模型
 @param pathName 存路径
 */
- (BOOL)saveModelWithPath:(NSString *)pathName;
/*
  根据path取出模型
  @param pathName 存路径
 */
+ (id)getModelWithPath:(NSString *)pathName;
/*
  存一个模型数组，此模型数组内模型必须都继承于本类
  @param arr 存数组 通讯录一般用的到
  @param pathName 存路径
 */
+ (void)saveModelArrWithArr:(NSArray *)arr WithPath:(NSString *)pathName;
/*
  取出存到本地的模型数组
 @param pathName 取出存到本地路径
 */
+ (NSArray *)getModelArrWithPath:(NSString *)pathName;
/*
  根据文件名删除沙盒
  @param fileName 删除沙河路径
 */
+ (void)removeFileByFileName:(NSString*)fileName;
@end
