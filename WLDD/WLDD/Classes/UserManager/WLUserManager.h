//
//  WLUserManager.h
//  WLDD
//
//  Created by 张庆勇 on 2019/12/7.
//  Copyright © 2019 物流. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WLUser.h"
NS_ASSUME_NONNULL_BEGIN

@interface WLUserManager : NSObject
/*
 * @brief 获取个人信息
 * @return userInfo
 */
+ (WLUser *)userInfo;
/*
 * @brief 保存个人信息
 * @return userInfo
 */
+ (void)saveUserInfo:(WLUser *)userInfo;

+(void)loginOut;

//是否登陆
+ (BOOL)is_Login;

//弹出登陆
+ (void)dj_showLoginVC:(UIViewController*)toViewController;
@end

NS_ASSUME_NONNULL_END
