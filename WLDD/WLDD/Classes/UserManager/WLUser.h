//
//  WLUser.h
//  WLDD
//
//  Created by 张庆勇 on 2019/12/7.
//  Copyright © 2019 物流. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WLCoding.h"
NS_ASSUME_NONNULL_BEGIN

@interface WLUser : WLCoding
@property (nonatomic,strong)NSString * userId;
@end

NS_ASSUME_NONNULL_END
