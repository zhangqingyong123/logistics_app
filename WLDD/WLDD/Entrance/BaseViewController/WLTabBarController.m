//
//  WLTabBarControllerViewController.m
//  WLDD
//
//  Created by 张庆勇 on 2019/12/7.
//  Copyright © 2019 物流. All rights reserved.
//

#import "WLTabBarController.h"
#import "WLLoginViewController.h"
#import "WLHomeViewController.h"
#import "WLAddressBookViewController.h"
#import "WLMyViewController.h"
#import "WLBaseNavigationViewController.h"
@interface WLTabBarController ()<UITabBarDelegate,UITabBarControllerDelegate>

@end

@implementation WLTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.delegate =self;
    //初始化子控制器
       WLHomeViewController*vc1 = [[WLHomeViewController alloc] init];
       WLAddressBookViewController *vc2 = [[WLAddressBookViewController alloc] init];
       WLMyViewController *vc3 = [[WLMyViewController alloc] init];
     
       
       [self addChildVc:vc1 title:@"工作" image:@"work_blk" selectedImage:@"work_blk" vcTag:0];
       [self addChildVc:vc2 title:@"通讯录" image:@"addressbook_blue" selectedImage:@"addressbook_blue" vcTag:1];
       [self addChildVc:vc3 title:@"我的" image:@"mine" selectedImage:@"mine" vcTag:2];
       
       
       self.selectedIndex = 0;
    [[UITabBar appearance]setBackgroundColor:WHITECOLOR];
}

/**
 *  添加一个子控制器
 *
 *  @param childVc       子控制器
 *  @param title         标题
 *  @param image         图片
 *  @param selectedImage 选中的图片
 */
- (void)addChildVc:(UIViewController *)childVc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage vcTag:(NSInteger)vcTag{
    
    // 设置子控制器的文字
    childVc.title = title; // 同时设置tabbar和navigationBar的文字
    childVc.tabBarItem.title = title; // 设置tabbar的文字
    childVc.navigationItem.title = title; // 设置navigationBar的文字
    
    // 设置子控制器的图片
    UIImage *img=[UIImage imageNamed:image];
    img=[img imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]; //不让图片渲染
    childVc.tabBarItem.image=img;

    childVc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childVc.tabBarItem.tag = vcTag;
    // 设置文字的样式
    NSMutableDictionary *textAttrs = [NSMutableDictionary dictionary];
    textAttrs[NSForegroundColorAttributeName] = ColorStr(@"#333333");
    textAttrs[NSFontAttributeName] = AutoFont(12);

    NSMutableDictionary *selectTextAttrs = [NSMutableDictionary dictionary];
    selectTextAttrs[NSForegroundColorAttributeName] =TABTITLECOLOR;
    //kRGBAColor(44, 143, 219, 1);;
    [childVc.tabBarItem setTitleTextAttributes:textAttrs forState:UIControlStateNormal];
    [childVc.tabBarItem setTitleTextAttributes:selectTextAttrs forState:UIControlStateSelected];
        
    // 先给外面传进来的小控制器 包装 一个导航控制器
    WLBaseNavigationViewController *nav = [[WLBaseNavigationViewController alloc] initWithRootViewController:childVc];
    // 添加为子控制器
    [self addChildViewController:nav];
}
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController NS_AVAILABLE_IOS(3_0){
    CABasicAnimation *pulse = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulse.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    pulse.duration = 0.2;   //time
    pulse.repeatCount = 1;  //重复次数
    pulse.autoreverses = YES;   //回到初始状态
    pulse.fromValue = [NSNumber numberWithFloat:2];   //初始伸缩倍数
    pulse.toValue = [NSNumber numberWithFloat:1.9];     //结束伸缩倍数
    UIImageView * tabbarSwappableImage = [self getBarButtonviewWithCurrentVC:viewController];
      [tabbarSwappableImage.layer addAnimation:pulse forKey:@"animateTransform"];
//    if (viewController.tabBarItem.tag == 4 || viewController.tabBarItem.tag == 3||viewController.tabBarItem.tag == 1 )
//    {
//
//        if (![DJLoginHelper sharedInstance].is_Login)
//        {
//            [[DJLoginHelper sharedInstance] dj_showLoginVC:self];
//            return NO;
//        }else
//        {
//            return YES;
//        }
//
//    }
    
    return YES;
}
- (UIImageView *)getBarButtonviewWithCurrentVC:(UIViewController *)currentViewController
{
    UIControl * tabbarButton = [currentViewController.tabBarItem valueForKey:@"view"];
    
    for (UIImageView * tabbatSwappableImage in tabbarButton.subviews)
    {
        return tabbatSwappableImage;
    }
    return nil;
}
@end
