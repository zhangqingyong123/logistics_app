//
//  BaseViewController.m
//  WLDD
//
//  Created by 张庆勇 on 2019/12/7.
//  Copyright © 2019 物流. All rights reserved.
//

#import "BaseViewController.h"
#import "WLAlertUtils.h"
@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:NO]; //设置隐藏
    [self.view addSubview:self.navView];
    [self.navView addSubview:self.leftButton];
    [self.navView addSubview:self.titleButton];
    [self.navView addSubview:self.rightButton];
    [self.navView addSubview:self.lineView];
    [self axcBaseRequestData];
    self.lineView.hidden = YES;
}
- (void)showLoadingAnimation
{
    [[WLAlertUtils sharedWLAlertUtils] show:kWindow];
}
- (void)stopLoadingAnimation
{
    [[WLAlertUtils sharedWLAlertUtils] stopWait];
}
- (UIView *)navView
{
    if (!_navView) {
        _navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, kStatusBarAndNavigationBarHeight)];
        _navView.backgroundColor = WHITECOLOR;
    }
    return _navView;
}
- (UIButton *)leftButton
{
    if (!_leftButton) {
        _leftButton = [[UIButton alloc] initWithFrame:CGRectMake(10, kStatusBarAndNavigationBarHeight -38, 30, 24)];
        [_leftButton setImage:[UIImage imageNamed:@"back"] forState:0];
       
        [_leftButton setTitleColor:MAINTITLECOLOR forState:0];
        [_leftButton addTarget:self action:@selector(leftItem:) forControlEvents:UIControlEventTouchUpInside];
        _leftButton.titleLabel.font =AutoFont(15);
       
    }
    return _leftButton;
}
- (void)leftItem:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (UIButton *)titleButton
{
    if (!_titleButton) {
        _titleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, kStatusBarAndNavigationBarHeight -40, 180, 32)];
        [_titleButton.titleLabel setFont:AutoFont(16)];
        [_titleButton setTitleColor:MAINTITLECOLOR forState:0];
        [_titleButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_titleButton addTarget:self action:@selector(centerIcon:) forControlEvents:UIControlEventTouchUpInside];
        _titleButton.centerX = _navView.centerX;
        
    }
    return _titleButton;
}
- (void)setTitleLable:(NSString *)titleLable
{
    [_titleButton setImage:[UIImage imageNamed:@""] forState:0];
    [_titleButton setTitle:titleLable forState:0];
}
- (void)centerIcon:(UIButton *)sender
{
    
}
- (UIButton *)rightButton
{
    if (!_rightButton) {
        _rightButton = [[UIButton alloc] initWithFrame:CGRectMake(UIScreenWidth -50, kStatusBarAndNavigationBarHeight -40, 46, 26)];
        [_rightButton setImage:[UIImage imageNamed:@"icon_header_chat"] forState:0];
        [_rightButton addTarget:self action:@selector(rightItem:) forControlEvents:UIControlEventTouchUpInside];
        [_rightButton setTitleColor:MAINTITLECOLOR forState:0];
        _rightButton.titleLabel.font = AutoFont(15);
    }
    return _rightButton;
}
- (void)rightItem:(UIButton *)sender
{    
}
- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, _navView.height-1, _navView.width, 0.5)];
        _lineView.backgroundColor = ColorStr(@"#DDDDDD");
    }
    return _lineView;
}
- (void)statusBarStyleIsDark:(BOOL)statusBarStyleIsDark
{
   if (statusBarStyleIsDark)
    {
        if (@available(iOS 13.0, *)) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDarkContent];
        } else {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        }

    }else
    {
      
        if (@available(iOS 13.0, *)) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        } else {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }
        
    }
}
- (void)SetTheLoadProperties:(BOOL)enabled{
    if (enabled) {
        //禁用侧滑手势方法
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }else
    {
        //禁用侧滑手势方法
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}
/**
 *  数据请求
 */
- (void)axcBaseRequestData{
    
}
@end
