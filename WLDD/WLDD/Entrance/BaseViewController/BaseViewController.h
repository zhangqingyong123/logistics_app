//
//  BaseViewController.h
//  WLDD
//
//  Created by 张庆勇 on 2019/12/7.
//  Copyright © 2019 物流. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController
@property (nonatomic,strong)UIView * navView;
@property (nonatomic,strong)UIView * lineView;
@property (nonatomic,strong)NSString * titleLable;
@property (nonatomic,strong)UIButton * leftButton;
@property (nonatomic,strong)UIButton * titleButton;
@property (nonatomic,strong)UIButton * rightButton;

/**
 *  上拉加载下拉刷新页数page
 */
@property(nonatomic,assign)int page;

/**
 *  基类请求数据
 */
- (void)axcBaseRequestData;
/**
 *  状态栏显示深色
 *  @param statusBarStyleIsDark 显示深色
 */
- (void)statusBarStyleIsDark:(BOOL)statusBarStyleIsDark;

/**
 *  左边返回按钮
 *  @param sender UIButton
 */
- (void)leftItem:(UIButton *)sender;
/**
 *  右边返回按钮
 *  @param sender UIButton
 */
- (void)rightItem:(UIButton *)sender;

/*
  开始动画
 */
- (void)showLoadingAnimation;
/*
 结束加载
 */
- (void)stopLoadingAnimation;

/**
 *  禁止左滑
 *  @param enabled 禁止左滑
 */
- (void)SetTheLoadProperties:(BOOL)enabled;
@end

NS_ASSUME_NONNULL_END
