//
//  WLTabBarControllerViewController.h
//  WLDD
//
//  Created by 张庆勇 on 2019/12/7.
//  Copyright © 2019 物流. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WLTabBarController : UITabBarController

@end

NS_ASSUME_NONNULL_END
