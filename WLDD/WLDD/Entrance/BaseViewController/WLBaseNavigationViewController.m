//
//  WLBaseNavigationViewController.m
//  WLDD
//
//  Created by 张庆勇 on 2019/12/7.
//  Copyright © 2019 物流. All rights reserved.
//

#import "WLBaseNavigationViewController.h"

@interface WLBaseNavigationViewController ()

@end

@implementation WLBaseNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
       //设置导航栏的颜色显示
       self.navigationBar.barTintColor = WHITECOLOR;
       [self.navigationBar setTitleTextAttributes:@{
                                                    NSFontAttributeName : [UIFont systemFontOfSize:16],
                                                    NSForegroundColorAttributeName :ColorStr(@"#333333")}];
}

/**
 *  重写这个方法目的：能够拦截所有push进来的控制器
 *
 *  @param viewController 即将push进来的控制器
 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    if (self.viewControllers.count > 0) { // 这时push进来的控制器viewController，不是第一个子控制器（不是根控制器）
        /* 自动显示和隐藏tabbar */
        viewController.hidesBottomBarWhenPushed = YES;
        
        /* 设置导航栏上面的内容 */
        //设置导航栏标题字体颜色
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:0.6],NSFontAttributeName:[UIFont systemFontOfSize:18]} forState:UIControlStateNormal];
        
        //回到上一页
        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        backBtn.frame = CGRectMake(0, 0, 25, 25);
        backBtn.contentEdgeInsets = UIEdgeInsetsMake(2, -15, 0, 0);
        [backBtn setImage:[UIImage imageNamed:@"back"] forState:0];
        backBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        [backBtn addTarget:self action:@selector(backToFormerpage) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        viewController.navigationItem.leftBarButtonItem = leftBtn;
        
        
        //开启iOS7的滑动返回效果
        if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            //只有在二级页面生效
            if ([self.viewControllers count] >= 1) {
                self.interactivePopGestureRecognizer.delegate = (id) self;
            }
        }
    }
    
    [super pushViewController:viewController animated:animated];
}

- (void)backToFormerpage{
    
    [self popViewControllerAnimated:YES];
}


@end
