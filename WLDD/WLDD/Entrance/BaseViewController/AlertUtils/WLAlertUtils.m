//
//  EXAlertUtils.m
//  Exchange
//
//  Created by 张庆勇 on 2019/10/18.
//  Copyright © 2019 张庆勇. All rights reserved.
//

#import "WLAlertUtils.h"
#import "YQLightLab.h"
#define MainScreenRect [UIScreen mainScreen].bounds
@implementation WLAlertUtils
DEFINE_SINGLETON_FOR_CLASS(WLAlertUtils)
- (void)show:(UIView *)view;
{
    _bgView = [[UIView alloc] init];
    _bgView.frame=MainScreenRect;
    _bgView.backgroundColor=CLEARCOLOR;
    _bgView.tag = 10086;
    [view addSubview:_bgView];
    
    UIView * alphaView = [[UIView alloc]initWithFrame:CGRectMake(UIScreenWidth/2 -45, UIScreenHeight/2 -40, 90,90)];
    alphaView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    alphaView.layer.cornerRadius = 4;
    alphaView.layer.masksToBounds = YES;
    alphaView.tag = 10086;
    alphaView.center = view.center;
    [_bgView addSubview:alphaView];
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyleLarge)];
    [alphaView addSubview:activityIndicator];
    //设置小菊花的frame
    activityIndicator.frame= CGRectMake(10, 0, 70, 70);
    //设置小菊花颜色
    activityIndicator.color = TABTITLECOLOR;
    //设置背景颜色
    activityIndicator.backgroundColor = CLEARCOLOR;
    //刚进入这个界面会显示控件，并且停止旋转也会显示，只是没有在转动而已，没有设置或者设置为YES的时候，刚进入页面不会显示
    activityIndicator.hidesWhenStopped = NO;
    [activityIndicator startAnimating];
    
   
    
  
    
    YQLightLable *label = [[YQLightLable alloc] init];
    label.frame = CGRectMake(0, alphaView.height -28, alphaView.width, 20);
    label.text = @"加载中...";
    label.textColor = WHITECOLOR;
    label.font = SystemFont(12);
    label.lightColor = TABTITLECOLOR;
    label.DurationTime = 1;                  // 滚动时间
    label.direction = YQLightLableDirectionToRight;
    label.textAlignment = NSTextAlignmentCenter;                        // 开启闪烁
    [alphaView addSubview:label];
    WeakSelf
    if (@available(iOS 10.0, *)) {
        [NSTimer scheduledTimerWithTimeInterval:10 repeats:YES block:^(NSTimer * _Nonnull timer) {
            [weakSelf stopWait];
        }];
    } else {
       
    }
    
}
- (void)stopWait
{
       [_bgView removeFromSuperview];
   
}
@end
