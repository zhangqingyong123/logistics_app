//
//  EXAlertUtils.h
//  Exchange
//
//  Created by 张庆勇 on 2019/10/18.
//  Copyright © 2019 张庆勇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WLAlertUtils : NSObject
DEFINE_SINGLETON_FOR_HEADER(WLAlertUtils)
@property (nonatomic, strong) UIView * bgView;
- (void)show:(UIView *)view;
- (void)stopWait;
@end
