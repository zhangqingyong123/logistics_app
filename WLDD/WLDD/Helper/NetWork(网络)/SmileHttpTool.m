//
//  SmileHttpTool.m
//  chengguaApp
//
//  Created by 微笑吧阳光 on 2018/4/4.
//  Copyright © 2018年 SmileFans. All rights reserved.
//

#import "SmileHttpTool.h"
#import "AFNetworking.h"
#import "UIKit+AFNetworking.h"
#import "SmileHTTPSessionManager.h"
#import "SmileNetWorkState.h"
#import "YINetWorkAPIGenerate.h"
#import "sys/utsname.h"
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

#define KNetStatus @"status"
#define KNetData @"result"
#define KNetMsg @"message"

@interface SmileHttpTool ()

/**注释---200成功 204 成功没有数据 code message */
@property (nonatomic, strong) SmileHTTPSessionManager *manager;
@end
@implementation SmileHttpTool

- (SmileHTTPSessionManager *)manager {
    if (!_manager) {
        _manager = [SmileHTTPSessionManager shareManager];
    }
    return _manager;
}

//创建单例
+ (SmileHttpTool *) sharedInstance
{
    static  SmileHttpTool *sharedInstance = nil ;
    static  dispatch_once_t onceToken;
    dispatch_once (& onceToken, ^ {
        //初始化自己
        sharedInstance = [[self alloc] init];
        //实例化请求对象
        sharedInstance.manager = [SmileHTTPSessionManager shareManager];
    });
    return  sharedInstance;
}
#pragma mark - 单例模式请求API
#pragma mark - GET
- (void)GET:(NSString *)URLString
 parameters:(id)parameters
     origin:(BOOL)isOrigin
    success:(NetworkSuccessBlock)success
    failure:(NetworkFailureBlock)failure
{
//    if (!kStringIsEmpty([[DJLoginHelper sharedInstance] cj_UserUuid])) {
//        [self.manager.requestSerializer setValue:[[DJLoginHelper sharedInstance] cj_UserUuid] forHTTPHeaderField:@"userid"];
//         NSString  * order = [NSString stringWithFormat:@"%@order",MatchHOST_IP];
        
//    }else{
//         [self.manager.requestSerializer setValue:@"" forHTTPHeaderField:@"userid"];
//    }
    
    [self.manager GET:URLString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
       
        success(0,task,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(nil, error);
        }
    }];
}

- (void)GET_Data:(NSString *)URLString
 parameters:(id)parameters
     origin:(BOOL)isOrigin
    success:(NetworkSuccessWithTaskBlock)success
    failure:(NetworkFailureBlock)failure
{
//    if (!kStringIsEmpty([[DJLoginHelper sharedInstance] cj_UserUuid])) {
//        [self.manager.requestSerializer setValue:[[DJLoginHelper sharedInstance] cj_UserUuid] forHTTPHeaderField:@"userid"];
//    }else{
//        [self.manager.requestSerializer setValue:@"" forHTTPHeaderField:@"userid"];
//    }
    //  [self.manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"mobileType"];
    [self.manager GET:URLString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        success(0,task,responseObject);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(nil, error);
        }
    }];
}

#pragma mark - POST
- (void)POST:(NSString *)URLString
  parameters:(id)parameters
      origin:(BOOL)isOrigin
     success:(NetworkSuccessBlock)success
     failure:(NetworkFailureBlock)failure
{
//    [self.manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

    [self.manager POST:URLString parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"responseObjectresponseObjectresponseObjectresponseObjectresponseObjectresponseObject----%@",responseObject);
       // NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
//       NSError *errorData;
//        NSString* strdata = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];//在将NSString类型转为NSData
//         NSData * data =[strdata dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
//         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorData];
//        NSLog(@"responseObject---%@",dict);
//        NSDictionary *responseDict = [NSDictionary dictionaryWithDictionary:responseObject];
//        if (isOrigin) {
//            success(0,@"",responseDict);
//            return ;
//        }
//        //[responseDict[KNetStatus] integerValue];
//        NSInteger status = [responseObject jsonInteger:KNetStatus];
//        NSString * msg = responseDict[KNetMsg];
//        id data = responseDict[KNetData];
//        if (success) {
//            success(status,msg,data);
//        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(nil, error);
        }
    }];
}





#pragma mark - 请求API
+ (void)httpWork_get:(NSString *)url parameters:(NSMutableDictionary *)parameters progress:(void (^)(NSProgress *))progress success:(void (^)(id))success successOther:(void (^)(id))successOther failure:(void (^)(NSError *))failure endRfresh:(void (^)(void))endRfresh showLoadingView:(BOOL)showLoadingView {
    
    if ([SmileNetWorkState checkNetWorkState].isRightNetwork == YES) {
        if (showLoadingView) {
            //加载动画
            
        }
        
        [[[SmileHttpTool alloc] init].manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
            if (progress) {
                progress(uploadProgress);
            }
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (showLoadingView) {//隐藏动画
                
            }
            if (endRfresh) {
                endRfresh();
            }
            
            if (responseObject) {
            //NSLog(@"💗--responseObject--%@",responseObject);
                //具体看项目接口返回的层级
                NSInteger code = [[responseObject valueForKey:@"status"] integerValue];
                
                if (code == 200) {
                    
                    if (success) {
                        success(responseObject);
                    }
                    
                } else if (code == 100) {//比如全局统一的，直接在里面弹窗处理
                    
                } else {
                    
                    if (code == 204) {//需要弹窗提示充值
                        if (successOther) {
                            successOther(responseObject);
                        }
                    } else {
                        //提示吐司窗口
                    }
                }
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (showLoadingView) {//隐藏动画
                
            }
            if (endRfresh) {
                endRfresh();
            }
            if (failure) {
                failure(error);
            }
        }];
    } else {
        //没有网络
    }
    
    
}

+ (void)httpWork_post:(NSString *)url parameters:(NSMutableDictionary *)parameters progress:(void (^)(NSProgress *))progress success:(void (^)(id))success successOther:(void (^)(id))successOther failure:(void (^)(NSError *))failure endRfresh:(void (^)(void))endRfresh showLoadingView:(BOOL)showLoadingView {
    
    
//    responseObject--{
//        message = "\U767b\U5f55\U5931\U8d25";
//        result = "";
//        status = 404;
//    }
    if ([SmileNetWorkState checkNetWorkState].isRightNetwork == YES) {
        if (showLoadingView) {
            //加载动画
        }
        
        [[[SmileHttpTool alloc] init].manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
            if (progress) {
                progress(uploadProgress);
            }
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (showLoadingView) {//隐藏动画
                
            }
            if (endRfresh) {
                endRfresh();
            }
            
            if (responseObject) {
                //具体看项目接口返回的层级
                NSInteger code = [[responseObject valueForKey:@"status"] integerValue];
                
                if (code == 0) {
                    
                    if (success) {
                        success(responseObject);
                    }
                    
                } else if (code == 100) {//比如全局统一的，直接在里面弹窗处理
                    
                } else {
                    
                    if (code == 200) {//需要弹窗提示充值
                        if (successOther) {
                            successOther(responseObject);
                        }
                    } else {
                        //提示吐司窗口
                    }
                }
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (showLoadingView) {//隐藏动画
                
            }
            if (endRfresh) {
                endRfresh();
            }
            if (failure) {
                failure(error);
            }
        }];
    } else {
        //没有网络
    }
}

@end
