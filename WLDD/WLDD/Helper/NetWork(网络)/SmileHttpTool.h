//
//  SmileHttpTool.h
//  chengguaApp
//
//  Created by 微笑吧阳光 on 2018/4/4.
//  Copyright © 2018年 SmileFans. All rights reserved.
//

#import <Foundation/Foundation.h>
#define KNetworkSuccess 0
typedef void (^NetworkSuccessBlock)(NSInteger statusCode,NSString* message,id responseObject);
typedef void (^NetworkSuccessWithTaskBlock)(NSInteger statusCode,NSURLSessionDataTask *  task,id responseObject);
typedef void (^NetworkFailureBlock)( id responseObject,NSError * error);

@interface SmileHttpTool : NSObject

//创建单例
+ (SmileHttpTool *) sharedInstance;

#pragma mark -- 单例调用

- (void)httpWork_get:(NSString *)url parameters:(NSMutableDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

//------------------------------post请求
/*
 URLString  请求地址
 parameters 请求参数
 origin     是否返回源数据
 success    成功回调
 failure    失败回调
 */
- (void)POST:(NSString *)URLString
  parameters:(id)parameters
      origin:(BOOL)isOrigin
     success:(NetworkSuccessBlock)success
     failure:(NetworkFailureBlock)failure;


//------------------------------get请求
/*
 URLString  请求地址
 parameters 请求参数
 origin     是否返回源数据
 success    成功回调
 failure    失败回调
 */
- (void)GET:(NSString *)URLString
 parameters:(id)parameters
     origin:(BOOL)isOrigin
    success:(NetworkSuccessBlock)success
    failure:(NetworkFailureBlock)failure;


- (void)GET_Data:(NSString *)URLString
      parameters:(id)parameters
          origin:(BOOL)isOrigin
         success:(NetworkSuccessWithTaskBlock)success
         failure:(NetworkFailureBlock)failure;



#pragma mark -- 基类

/**
 * 基类,get请求
 * url：可能全局是固定的
 * parameters ： 参数
 * progress ： 进度
 * success ： 指定成功的code回调
 * successOther ： 相对于成功的code 其他的code做处理，可能是需要交互的弹出框或者其他操作
 * failure ： 请求失败做处理
 * endRfresh ： 结束刷新的
 * showLoadingView ： 是否显示加载动画
 */

+ (void)httpWork_get:(NSString *)url parameters:(NSMutableDictionary *)parameters progress:(void(^)(NSProgress * downloadProgress))progress  success:(void (^)(id responseObject))success successOther:(void (^)(id responseObject))successOther failure:(void (^)(NSError *error))failure endRfresh:(void (^)(void))endRfresh  showLoadingView:(BOOL)showLoadingView;

+ (void)httpWork_post:(NSString *)url parameters:(NSMutableDictionary *)parameters progress:(void(^)(NSProgress * downloadProgress))progress  success:(void (^)(id responseObject))success successOther:(void (^)(id responseObject))successOther failure:(void (^)(NSError *error))failure endRfresh:(void (^)(void))endRfresh  showLoadingView:(BOOL)showLoadingView;




@end
