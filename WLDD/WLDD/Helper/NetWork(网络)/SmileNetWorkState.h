//
//  SmileNetWorkState.h
//  chengguaApp
//
//  Created by 微笑吧阳光 on 2018/4/4.
//  Copyright © 2018年 SmileFans. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SmileNetWorkState : NSObject
///
@property (nonatomic, assign) BOOL isRightNetwork;

+ (instancetype)checkNetWorkState;
@end
