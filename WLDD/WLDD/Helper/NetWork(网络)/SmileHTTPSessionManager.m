//
//  SmileHTTPSessionManager.m
//  chengguaApp
//
//  Created by 微笑吧阳光 on 2018/4/4.
//  Copyright © 2018年 SmileFans. All rights reserved.
//

#import "SmileHTTPSessionManager.h"

@implementation SmileHTTPSessionManager
+ (instancetype)shareManager {
    static SmileHTTPSessionManager *_manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _manager = [[SmileHTTPSessionManager alloc] init];
        //AFJSONResponseSerializer * responseSerializer =[AFJSONResponseSerializer serializerWithReadingOptions NSJSONReadingAllowFragments
        //暂定10s超时
       
        _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
         [_manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"mobileType"];
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _manager.requestSerializer.stringEncoding = kCFStringEncodingUTF8;
        _manager.requestSerializer.timeoutInterval = 600;
        _manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
         [_manager.requestSerializer setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        _manager.responseSerializer.acceptableContentTypes =[NSSet  setWithObjects:@"application/json",@"text/json",@"text/javascript",@"text/plain",@"text/html",@"text/html",@"image/jpeg",@"image/png",@"application/octet-stream",nil];
     
    });

    return _manager;
}

@end
