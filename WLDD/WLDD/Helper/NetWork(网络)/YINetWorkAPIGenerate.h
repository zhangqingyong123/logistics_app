//
//  YINetWorkAPIGenerate.h
//  ksofficeApp
//
//  Created by SuperMan on 2018/3/8.
//

#import <Foundation/Foundation.h>
//是不是测试环境
#define APITEST [[NSBundle mainBundle].infoDictionary[@"APITEST"] boolValue]
@interface YINetWorkAPIGenerate : NSObject
+(YINetWorkAPIGenerate*)sharedInstance;
-(NSString*)API:(NSString*)apiName;
//没有？
-(NSString*)APINomark:(NSString*)apiName;

- (NSString *)JCAPI:(NSString *)apiName;

-(NSString*)APIName:(NSString*)apiName;
@end
