//
//  YINetWorkAPIGenerate.m
//  ksofficeApp
//
//  Created by SuperMan on 2018/3/8.
//

#import "YINetWorkAPIGenerate.h"


const NSString * defaultNetworkHost =@"1ccfw12.top/richman";
//@"edf0531.com/richman";
//const NSString * defaultNetworkHostTest = @"65cb23bc.ngrok.io/richman";
const NSString * defaultNetworkHostTest =  @"119.81.171.118:8080";
//const NSString * defaultNetworkHostTest = @"65cb23bc.ngrok.io";
//请求不了就用IP的 http://119.81.171.118:8080/   http://test.ydfbet.net:8080/ 这个
static NSString* const apiFileName = @"GrabDollConfig";
static NSString* const apiFileExtension = @"json";


@implementation YINetWorkAPIGenerate


+(YINetWorkAPIGenerate*)sharedInstance{
    static YINetWorkAPIGenerate* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(& onceToken,^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

//获取api字典
+(NSDictionary*)apiDictionary
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:apiFileName ofType:apiFileExtension];
    NSData* data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    return dic;
}
-(NSString*)API:(NSString*)apiName{
    NSDictionary* configDict = nil;
    configDict = [[self class] apiDictionary];
    const NSString *host = APITEST ? defaultNetworkHostTest : defaultNetworkHost;
    
    NSDictionary *dic = [configDict objectForKey:apiName];
    const  NSString* apiProtocol = APITEST? @"http": @"https";
    //拼接url
    NSString *apiUrl = [NSString stringWithFormat:@"%@://%@",apiProtocol,host];
    NSString *control = dic[@"control"];
    if (control && ![control isEqual:[NSNull null]] && control.length > 0) {
        apiUrl = [apiUrl stringByAppendingFormat:@"/%@", control];
    }
    NSString *action = dic[@"action"];
    if (action && ![action isEqual:[NSNull null]] && action.length > 0) {
        apiUrl = [apiUrl stringByAppendingFormat:@"/%@", action];
    }
    apiUrl = [apiUrl stringByAppendingString:@"?"];
    
    return apiUrl;
}
-(NSString*)APIName:(NSString*)apiName{
    NSDictionary* configDict = nil;
    configDict = [[self class] apiDictionary];
    const NSString *host = APITEST ? defaultNetworkHostTest : defaultNetworkHost;
    
    NSDictionary *dic = [configDict objectForKey:apiName];
   const  NSString* apiProtocol = APITEST? @"http": @"https";
    //拼接url
    NSString *apiUrl = [NSString stringWithFormat:@"%@://%@",apiProtocol,host];
    NSString *control = dic[@"control"];
    if (control && ![control isEqual:[NSNull null]] && control.length > 0) {
        apiUrl = [apiUrl stringByAppendingFormat:@"/%@", control];
    }

    apiUrl = [apiUrl stringByAppendingString:@"/"];
    NSString * str = kFormat(@"%@%@", apiUrl,apiName);
    return str;
}
-(NSString*)APINomark:(NSString*)apiName{
    NSDictionary* configDict = nil;
    configDict = [[self class] apiDictionary];
    const NSString *host = APITEST ? defaultNetworkHostTest : defaultNetworkHost;
    
    NSDictionary *dic = [configDict objectForKey:apiName];
   const  NSString* apiProtocol = APITEST? @"http": @"https";
    //拼接url
    NSString *apiUrl = [NSString stringWithFormat:@"%@://%@",apiProtocol,host];
    NSString *control = dic[@"control"];
    if (control && ![control isEqual:[NSNull null]] && control.length > 0) {
        apiUrl = [apiUrl stringByAppendingFormat:@"/%@", control];
    }
    NSString *action = dic[@"action"];
    if (action && ![action isEqual:[NSNull null]] && action.length > 0) {
        apiUrl = [apiUrl stringByAppendingFormat:@"/%@", action];
    }
    
    return apiUrl;
}
//- (NSString *)JCAPI:(NSString *)apiName
//{
//    NSString * url = [NSString stringWithFormat:@"%@%@",HOST_IP,apiName];
//    return url;
//}
@end
