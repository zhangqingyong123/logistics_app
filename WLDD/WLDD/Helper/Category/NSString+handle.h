//
//  NSString+handle.h
//  yuzhilai
//
//  Created by jiang on 2017/6/12.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NSString (handle)

- (NSString *)filterEmoji:(NSString *)string;

- (CGSize)stringSizeWithFont:(UIFont *)font;

- (NSString *)MD5String;

- (NSUInteger)getBytesLength;

- (NSString *)stringByDeletingPictureResolution;
//普通size
-(CGSize)getStrSize:(NSString*)str font:(UIFont*)font size:(CGSize)size;
//富文本size
-(CGSize)getStrsize:(NSString*)str font:(UIFont*)font lineSpace:(CGFloat)space firstLineSpace:(CGFloat)firstLineSpace size:(CGSize)size ;
+(NSString*)UUID;
//时间转换 几天前 几小时前 几分钟前
+ (NSString *)theTime:(NSString *)issueTime;
- (BOOL)isMobileNumber;
+(BOOL)isMobileNumChack:(NSString *)input;
- (NSString *)URLDecode;
- (NSString *)URLEncode;
- (NSString*)weekdayStringFromDate:(NSDate*)inputDate;

//传入 秒 得到 xx分钟xx秒
+(NSString *)getMMSSFromSS:(NSString *)totalTime;
//uCity时间转换
+(NSString*)getTheTimeWithTimeStamp:(NSString*)timeStamp;

+ (NSString *)timeWithTimeIntervalString:(NSString *)timeString;
+(NSString *)MD5ForLower32Bate:(NSString *)str;
//返回自定义格式的时间
+(NSString*)getTheTimeWithTimeStamp:(NSString*)timeStamp formate:(NSString *)formateStr;
//转换固定格式的时间
+(NSString *)transformTime:(NSTimeInterval)time withFormat:(NSString *)formatter;

+(NSString *)timeWithTimeIntervalStringHMS:(NSString *)timeString;


+ (NSString *)timeWithTimeIntervalStringYMD:(NSString *)timeString;

+ (NSString *)updateTimeForRow:(NSString *)createTimeString;

+ (NSString *)timeWithTimeIntervalStringHMS:(NSString *)timeString formatter:(NSString *)format;


- (NSString *)URLDecodedString;

//是否是银行卡号
-(BOOL)validateBankCard;
- (BOOL)isBankCardNumber:(NSString *)cardNum;
//Luhn算法
-(BOOL)isValidCardNumber:(NSString *)cardNumber;
-(BOOL)checkBankCardNumber:(NSString*)cardNumber;
@end
